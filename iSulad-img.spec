%global _version 2.0.1
%global _release 20201125.171836.git35177b29
Name:       iSulad-img
Version:    %{_version}
Release:    %{_release}
Summary:    a tool for downloading iSulad images


Group:      Applications/System
License:    Mulan PSL v2
URL:        https://gitee.com/openeuler/iSulad-img
Source0:    https://gitee.com/openeuler/iSulad-img/repository/archive/v%{version}.tar.gz

Patch6001: 0001-enable-strip-and-ftrapv.patch
Patch6002: 0002-do-not-strip-when-make-strip-it-when-make-rpm-if-nec.patch

BuildRequires:  golang >= 1.8.3
BuildRequires:  gpgme gpgme-devel
BuildRequires:  device-mapper-devel

%description
A tool for downloading iSulad images, written in go language

%prep
%autosetup -n %{name} -Sgit -p1

# apply the patchs
cp ./patch/* ./
cat series-patch.conf | while read line
do
        if [[ $line == '' || $line =~ ^\s*# ]]; then
                continue
        fi
        patch -p1 -F1 -s < $line
done
cd -

%build
make %{?_smp_mflags}

%install
install -d $RPM_BUILD_ROOT/%{_bindir}
install -m 0755 ./isulad-img %{buildroot}/%{_bindir}/isulad-img
install -d $RPM_BUILD_ROOT/%{_sysconfdir}/containers
install -m 0644 ./default-policy.json $RPM_BUILD_ROOT/%{_sysconfdir}/containers/policy.json

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/isulad-img
%{_sysconfdir}/*

%changelog
* Wed Nov 25 2020 wangfengtu <wangfengtu@huawei.com> - 2.0.1-20201125.171836.git35177b29
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: enable ftrapv

* Tue Sep 10 2020 YoungJQ <yangjiaqi11@huawei.com> - 2.0.1-20200910.145214.git7e4f382a
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add debug package

* Wed Sep 02 2020 YoungJQ <yangjiaqi11@huawei.com> - 2.0.1-20200902.114147.git497e740a
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: modify source0 address
